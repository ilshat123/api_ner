#!/usr/bin/env bash
# PROJECT_DIR="$ROOT_PATH"
PROJECT_DIR=/app/bert
MODEL_DIR=$1
DATA_DIR=$2
OUTPUT_DIR=$3

echo $PROJECT_DIR

mkdir $OUTPUT_DIR
rm $PROJECT_DIR/ner_outputs/token_test.txt
rm $PROJECT_DIR/ner_outputs/test.tsv

python3 $PROJECT_DIR/data_processing_utils/data2conll.py --in_file $PROJECT_DIR/data/pars_res.json

CUDA_VISIBLE_DEVICES=0 python3 $PROJECT_DIR/bert/run_ner.py --do_eval=False --do_train=False --do_predict=True \
                              --vocab_file=$MODEL_DIR/vocab.txt --bert_config_file=$MODEL_DIR/bert_config.json \
                              --init_checkpoint=$MODEL_DIR/bert_model.ckpt \
                              --num_train_epochs $4  --data_dir=$DATA_DIR  \
                              --output_dir=$PROJECT_DIR/ner_outputs  \
                              --save_checkpoints_steps 5000

python3 $PROJECT_DIR/bert/biocodes/detok.py --tokens $OUTPUT_DIR/token_test.txt --label $OUTPUT_DIR/label_test.txt \
                                           --save_to $OUTPUT_DIR/predicted_biobert.txt
python3 $PROJECT_DIR/data_processing_utils/combine.py --gold_data $DATA_DIR/test.tsv \
                                                     --predicted $OUTPUT_DIR/predicted_biobert.txt \
                                                     --save_to $OUTPUT_DIR/predicted_conll.txt
$PROJECT_DIR/evaluation_scripts/./conlleval < $OUTPUT_DIR/predicted_conll.txt > $OUTPUT_DIR/eval_results.txt

python3 $PROJECT_DIR/data_processing_utils/conll2json.py --in_file $OUTPUT_DIR/predicted_biobert.txt

rm $PROJECT_DIR/ner_outputs/predict.tf_record
