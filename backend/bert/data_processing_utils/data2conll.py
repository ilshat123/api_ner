import json
import nltk
from nltk.tokenize import wordpunct_tokenize, sent_tokenize
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-in_file", "--in_file", type=str)
args = parser.parse_args()

nltk.download('punkt')
f = open(args.in_file, encoding='utf-8')
out_file = open("/app/bert/data/rudrec_annotated/test.tsv", 'w+', encoding='utf-8')
documents = json.load(f)
for document in documents:
    sentences = sent_tokenize(document)
    for sentence in sentences:
        tokens = wordpunct_tokenize(sentence)
        bio_tag = 'O'
        for token in tokens:
            out_file.write("{}\t{}\n".format(token, bio_tag))
        out_file.write("\n")
out_file.close()

