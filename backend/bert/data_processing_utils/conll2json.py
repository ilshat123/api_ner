import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-in_file", "--in_file", type=str)
args = parser.parse_args()
f = open(args.in_file, encoding='utf-8')
out_dict = {"data": []}
texts = ['']
ent = [[]]
types = [[]]
all_conll = f.read().split("\n\n")
print(all_conll[-1])
for sent in all_conll:
    wordent = sent.split("\n")
    s_ent = ''
    ent_flag = False
    for we in wordent:
        if we == '':
          continue
        word = we.split(" ")[0]
        label = we.split(" ")[1]
        texts[-1] = texts[-1] + word + " "
        if label[0] == 'B':
            if s_ent != '':
                ent[-1].append(s_ent)
                s_ent = ''
            s_ent = s_ent + word + ' '
            ent_flag = True
            types[-1].append(label.split("-")[-1])
            continue
        if label[0] == 'O':
            ent_flag = False
        if ent_flag:
            s_ent = s_ent + word + ' '
    if s_ent != '':
        ent[-1].append(s_ent)
    ent.append([])
    types.append([])
    texts.append('')
for en, ty, te in zip(ent, types, texts):
    d = {"text": te, "entities": []}
    for e, t in zip(en, ty):
        d["entities"].append({"text": e, "type": t})
    out_dict["data"].append(d)

with open('/app/bert/data/result.json', 'w+', encoding='utf-8') as fp:
    json.dump(out_dict, fp, ensure_ascii=False)
