import json
import subprocess
import traceback

from spacy import displacy

from core import settings


def bert_func(data):
    # print(data)
    results = {'data': []}
    groups = [data[i:i + 1000] for i in range(0, len(data), 1000)]
    # разбивает текста на группы по 1000 элементов, чтобы не нейронка не сломалась
    for group in groups:
        with open(settings.input_file, 'w', encoding='utf-8') as file:
            file.write(json.dumps(group, ensure_ascii=False))

        res = subprocess.call(settings.run_sh_command, shell=True)

        with open(settings.output_file, 'r', encoding='utf-8') as file:
            result = file.read()
            result = json.loads(result)
            results['data'].extend(result['data'])

    return results


def create_entities_for_view(original_text: str, entities):
    ents = []
    for entity in entities:
        word_st = original_text.index(entity['text'])
        word_fs = word_st + len(entity['text'])
        ents.append({'start': word_st, 'end': word_fs, 'label': entity['type']})
    return ents


def create_entities_for_view_texts(original_texts):
    # original_texts [{"text": "Первые вакцины вкололи 5 дней назад , будем скидывать сюда результаты анализов и
    # мониторить образование антител ", "entities": [{"text": "вакцины", "type": "Drugclass"}]}, ..]
    exs = []
    for orig in original_texts:
        try:
            # print('orig', orig)
            text = orig['text']
            entities = orig['entities']
            entities = create_entities_for_view(text, entities)
            ex = {'text': text,
                   'ents': entities,
                   "title": None}
            ex = displacy.render([ex], style="ent", manual=True)
            exs.append(ex)
        except:
            pass
            # print(traceback.format_exc())
    return exs
