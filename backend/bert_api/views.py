import json
import subprocess
import traceback

from django.contrib import messages
from django.http import JsonResponse, HttpRequest, HttpResponse
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import FormView
from spacy import displacy

from bert_api.forms import TextForm
from bert_api.services import bert_func, create_entities_for_view_texts
from core import settings


class BertView(View):

    def post(self, request, *args, **kwargs):
        try:
            text = str(request.body, encoding='utf-8')
            data = json.loads(text)['texts']
            result = bert_func(data)
            return JsonResponse(result, json_dumps_params={'ensure_ascii': False})
        except:
            print(traceback.format_exc())
            return JsonResponse({'error': str(traceback.format_exc())}, safe=False)


class DemoView(FormView):
    form_class = TextForm
    template_name = 'base.html'
    success_url = reverse_lazy('bert_api:demo_view')
    html = None

    def form_valid(self, form):

        text = form.cleaned_data['texts']  # здесь подается только 1 текст
        # data = json.loads(texts)
        # print(text)
        result = bert_func([text])
        # print(result)
        exs = create_entities_for_view_texts(result['data'])
        exs = '<div>____</div>'.join(exs)
        # print(exs)
        # self.html = displacy.render(exs, style="ent", manual=True)
        messages.add_message(self.request, message=exs, level=messages.INFO)
        return super().form_valid(form)

    def get_success_message(self, cleaned_data):
        return self.html