from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms


class TextForm(forms.Form):
    texts = forms.CharField(label='Введите текст', widget=forms.Textarea(attrs={'rows': 4, 'cols': 20}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Run'))
