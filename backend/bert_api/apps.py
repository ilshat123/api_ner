from django.apps import AppConfig


class ProjectConfig(AppConfig):
    name = 'bert_api'
